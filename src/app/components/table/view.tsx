import { AvailableTables, Store } from 'app/store/reducers/types';
import { IStore, ITableData } from 'app/store/types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import tableData from 'static/data/data.json';
import { ITable, Table } from './table';

interface IProps extends ITable {
    chosenTable: AvailableTables;
    tables?: Record<AvailableTables, ITable>;
}

class View extends Component<IProps> {
    renderTable = (props: ITable & { chosenTable: AvailableTables }) =>  <Table {...props} />;

    render() {
       const { tables, columns, chosenTable, data } = this.props;

       if (!tables) {
          return this.renderTable({ columns, chosenTable, data });
       }

       // @ts-ignore
       const tablesAsArray: Array<[AvailableTables, ITable]> = Object.entries(tables);

       return tablesAsArray.map(([tableName, tableData]) => (
          this.renderTable({ ...tableData, chosenTable: tableName }))
       );
    }
}

const mapStateToProps = (store: IStore) => {
   const chosenTable = store[Store.chosenTable] as AvailableTables;
   // @ts-ignore
   const { columns, data, tables } = tableData[chosenTable] as ITableData;

   return ({ columns, data, tables, chosenTable: store.chosenTable });
};

export const TableView = connect(mapStateToProps)(View);
