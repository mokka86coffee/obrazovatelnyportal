import { IAnyObject } from 'types';

// FIXME костыль
let bgColor = 'white';
export const getRowBgColor = (row: any) => {
   const [info] = row;
   if (info?.backgroundColor) {
      bgColor = info.backgroundColor;
   }

   return bgColor;
};

export const getRowFromDataWithColumns = (row: IAnyObject, columns: string[]) => {
   // для кейсов "only"
   if (row.only) {
      bgColor = row.params.bgColor;

      return [
         {
            title: row.only,
            params: {
               style: {
                  backgroundColor: bgColor,
                  fontSize: '20px',
                  minWidth: '100%',
                  maxWidth: '100%',
               }
            }
         }
      ];
   }

   return columns.map(title => (
      {
         title: row[title],
         params: {
            style: {
               backgroundColor: bgColor,
            }
         }
      }));
};

const withCommaDeleted = (value: string) => value.replace(/,/g, '');
const isNumber = (value: string) => !Number.isNaN(Number(value));

export const getSortedByField = (rows: IAnyObject[], field: string, reverse?: boolean) => {
   const unlinkedRows = [...rows];
   const sortedRows = unlinkedRows
      .sort(({ [field]: value1 }, { [field]: value2 }) => {
         if (value1 === value2) {
            return 0;
         }

         const correctedValue1 = withCommaDeleted(value1);
         const correctedValue2 = withCommaDeleted(value2);

         if (isNumber(correctedValue1) && isNumber(correctedValue2)) {
            return Number(correctedValue1) - Number(correctedValue2);
         }

         return value1 < value2 ? -1 : 1;
      });

   return reverse ? sortedRows.reverse() : sortedRows;
};
