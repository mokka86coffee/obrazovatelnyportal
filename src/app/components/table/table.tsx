import { afterChartImage } from 'app/components/table/constants';
import { getRowBgColor, getRowFromDataWithColumns, getSortedByField } from 'app/components/table/utils';
import { AvailableTables, Store } from 'app/store/reducers/types';
import { ITableData } from 'app/store/types';
import React, { Component } from 'react';
import { Image } from 'static/images';
import { IAnyObject } from 'types';
import { v4 as uuidv4 } from 'uuid';
import { Wrap, TableBody, TableHead, TableWrap, TableCell, TableRow, ChartWrap, TableTitle } from './ui';


enum SortingPrefixes {
    AtoZ= 'a-z',
    ZtoA= 'z-a',
}

export interface ITable {
    columns: ITableData['columns'];
    data: ITableData['data'];
}

interface IProps extends ITable {
    chosenTable: AvailableTables;
}

interface IState {
    sortBy: string;
    sortType: SortingPrefixes;
}

export class Table extends Component<IProps, IState> {
    state = {
       sortBy: '',
       sortType: SortingPrefixes.AtoZ,
    }

    setsortBy = (newSortBy: string) => {
       const { sortBy, sortType } = this.state;


       const isSameSortType = newSortBy === sortBy;
       const newSortType = isSameSortType
          ? sortType === SortingPrefixes.AtoZ ? SortingPrefixes.ZtoA : SortingPrefixes.AtoZ
          : SortingPrefixes.AtoZ;

       this.setState({ sortBy: newSortBy, sortType: newSortType });
    }

    renderHeaderCells = () => {
       const { columns } = this.props;
       const { sortBy, sortType } = this.state;

       return columns.map(title => (
          <TableCell
             inHeader
             isActive={title === sortBy}
             onClick={() => this.setsortBy(title)}
             fromAtoZ={sortType === SortingPrefixes.AtoZ}
             key={title}
          >
             {title}
          </TableCell>
       ));
    }

    renderCells = (row: Array<{ title: string; params: IAnyObject }>) => {
       return row.map(({ title, params }) => (
          <TableCell {...params} key={uuidv4()}>
             {title}
          </TableCell>
       ));
    }

    get sortedRows() {
       const { sortBy, sortType } = this.state;
       const { data, columns } = this.props;

       const sortedRows = getSortedByField(data, sortBy, sortType === SortingPrefixes.ZtoA);

       return sortedRows.map(row => getRowFromDataWithColumns(row, columns));
    }

    renderRows = () => {
       return this.sortedRows.map(row => {
          const style = {
             backgroundColor: getRowBgColor(row),
          };

          return (
             <TableRow style={style} key={uuidv4()}>
                {this.renderCells(row)}
             </TableRow>
          );
       });
    }

    render() {
       const { chosenTable } = this.props;
       const chartImage = afterChartImage[chosenTable];
       // @ts-ignore
       const ChartImageComponent = Image[chartImage];


       return (
          <>
             <TableTitle>{chosenTable}</TableTitle>
             <Wrap>
                <TableWrap>
                   <TableRow header>
                      {this.renderHeaderCells()}
                   </TableRow>
                   {this.renderRows()}
                </TableWrap>
             </Wrap>
             {chartImage && <ChartWrap><ChartImageComponent /></ChartWrap>}
          </>
       ); 
    }
}
