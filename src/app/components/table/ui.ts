import { flexAlignCenter } from 'app/styled';
import styled, { css } from 'styled-components';

export const TableTitle = styled.h5`
    margin-bottom: 20px;
    padding: 0 20px;
    font-size: 18px;
    font-weight: 400;
    text-align: center;
    
    .mobile & {
        font-size: 14px;
    }
`;

export const TableCell = styled.div<{ isActive?: boolean; inHeader?: boolean; fromAtoZ?: boolean }>`
    position: relative;
    min-width: 200px;
    max-width: 200px;
    padding: 10px;
    text-align: center;
    font-size: 12.8px;
    line-height: 21px;
    
    ${({ inHeader }) => inHeader && css`
        cursor: pointer;
    `}
    
    ${({ isActive, fromAtoZ }) => isActive && css`
      &::after {
          content: '▼';
          position: absolute;
          top: 50%;
          transform: ${fromAtoZ ? 'translateY(-50%) scaleY(.6)' : 'translateY(-50%) scaleY(.6) rotateZ(180deg)'};
          right: -6px;
          font-size: 20px;
      }
    `}
`;

export const TableRow = styled.div<{ header?: boolean }>`
    width: max-content;
    ${flexAlignCenter};
    align-items: stretch;
    
    ${({ header }) => header && css`
        background-color: lightsteelblue;
        font-weight: 400;
        & ${TableCell} {
        }
    `};
`;

export const TableHead = styled.thead`
  
`;

export const TableBody = styled.tbody`
  
`;

export const TableWrap = styled.div`
`;

export const ChartWrap = styled.div`
  & > img {
      max-width: 100%;
  }
`;

export const Wrap = styled.div`
    width: 100%;
    overflow-x: scroll;
    &::-webkit-scrollbar-thumb {
        border-radius: 10px;
        background-color: lightsteelblue;
    }
    &::-webkit-scrollbar {
        width: 8px;
    };
    
      .mobile & {
          margin-bottom: 20px;
      }
`;
