import React, { Component } from 'react';
import { Logo, Title, Wrap } from './ui';
import { Menu } from './menu';

export class Header extends Component {
    static displayName = 'Header';

    render() {
       return (
          <Wrap>
             <Logo alt="Информационно-аналитическая система мониторинга национальных проектов" />
             <Title>Информационно-аналитическая система мониторинга национальных проектов</Title>
             <Menu />
          </Wrap>
       );
    }
}
