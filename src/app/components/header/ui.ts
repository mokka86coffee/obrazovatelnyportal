import styled from 'styled-components';
import { Image } from 'static/images';

export const Logo = styled(Image.Logo)`
  .mobile & {
      width: 172px;
  }
`;

export const Title = styled.h1`
    margin-bottom: 20px;
    padding-bottom: 10px;
    font-size: 26px;
    text-align: center;
    font-weight: bold;
    border-bottom: 4px solid #0071BC;
    
    .mobile & {
        max-width: 280px;
        font-size: 16px;
        text-align: left;
    }
`;

export const Wrap = styled.div`
  position: relative;
`;
