import { flexAlignCenter } from 'app/styled';
import styled, { css } from 'styled-components';

export const MenuToggler = styled.div`
  display: none;
  position: absolute;
  top: 0;
  right: 10px;
  background-color: var(--main-dark-blue);
  z-index: 3;
  
  .mobile & {
    display: block;
    height: 68px;
    width: 68px;
    
    &::before,
    &::after {
      content: '';
      position: absolute;
      top: 40%;
      right: 30%;
      height: 2px;
      width: 40%;
      border: 2px solid transparent;
    }
    &::before {
      height: 6px;
      border-top-color: var(--main-white);
      border-bottom-color: var(--main-white);
    }
    &::after {
      top: calc(40% + 16px);
      width: 30%;
      border-top-color: var(--main-white);
    }
  }
`;

export const SubMenuItem = styled.li<{ isActive: boolean }>`
    width: 34%;
    min-height: 90px;
    max-height: 90px;
    padding: 16px 32px;
    display: flex;
    align-items: center;
    line-height: 18px;
    overflow: hidden;
    text-overflow: ellipsis;
    background-color: var(--main-blue);
    
    &:hover {
      background-color: var(--main-blue-hover);
    }
    
    ${({ isActive }) => isActive && css`
        background-color: var(--main-blue-active);
    `}
    
    .mobile & {
        width: 77%;
        min-height: 30px;
        max-height: 30px;
        justify-content: center;
    }
`;

export const SubMenuWrap = styled.ul`
  display: none;
  position: absolute;
  top: 100%;
  left: 0;
  max-height: 400px;
  width: 200%;
  
  .mobile & {
    top: 20px;
    position: relative;
    max-height: max-content;
    max-width: 100%;
  }
`;

export const MenuItem = styled.li<{ withSubMenu?: any, isDisabled?: any }>`
  position: relative;
  height: 100%;
  ${flexAlignCenter};
  
  ${({ isDisabled }) => isDisabled && css`
    opacity: .4;
  `}
      
  &:hover {
      background-color: var(--main-blue);
  }
  
  & > a {
      color: #fff;
      display: block;
      padding: 16px 28px;
  }
  
  ${({ withSubMenu }) => withSubMenu && css`
      &::after {
          content: '▼';
          position: absolute;
          top: 50%;
          transform: translateY(-50%) scaleY(.6);
          right: 10px;
          font-size: 20px;
        
          .mobile & {
            right: 10px;
          }
      }
      
      &:hover {
        background-color: var(--main-blue);

        & ${SubMenuWrap} {
            display: flex;
            flex-direction: column;
            flex-wrap: wrap;
        }
        
          .mobile &::after {
              content: '';
          }
      }
  `};
  
  .mobile & {
    width: 100%;
    max-width: 100%;
    display: block;
  }
`;

export const MenuWrap = styled.ul<{ isVisible: boolean }>`
    ${flexAlignCenter};
    position: relative;
    z-index: 2;
    
    height: 72px;
    max-width: 100%;
    margin-bottom: 32px;
    
    font-size: 12px;
    text-align: center;
    letter-spacing: 0.01em;
    
    background-color: var(--main-dark-blue);
    color: var(--main-white);
    cursor: pointer;
  
    &:active {
        background-color: var(--main-dark-blue);
    }

    .mobile & {
        position: absolute;
        top: 0;
        width: 100%;
        left: 0;
        height: auto;
        padding-top: 60px;
        padding-bottom: 30px;
        flex-wrap: wrap;
        z-index: 2;

        display: none;
        ${({ isVisible }) => isVisible && css`
            display: flex;
        `}
    }
`;
