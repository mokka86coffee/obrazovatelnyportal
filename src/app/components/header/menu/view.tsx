import React, { Component } from 'react';
import { connect } from 'react-redux';
import { IStore } from 'app/store/types';
import { AvailableTables } from 'app/store/reducers/types';
import { chooseTable } from 'app/store/actions';
import { ISubMenuItem, MENU_STRUCTURE } from './constants';
import { MenuWrap, MenuItem, SubMenuItem, SubMenuWrap, MenuToggler } from './ui';

interface IProps {
    isMobile: boolean;
    chosenTable: AvailableTables;
    chooseTable(title: AvailableTables): void;
}

class _Menu extends Component<IProps> {
    state = {
       isVisible: false
    }

    componentDidMount() {
       if (!this.props.isMobile) {
          this.setState({ isVisible: true });
       }
    }

    toggleMenu = () => this.setState({ isVisible: !this.state.isVisible });

    chooseTable = (title: AvailableTables) => {
       const { chooseTable, isMobile } = this.props;
       chooseTable(title);

       if (isMobile) {
          this.toggleMenu();
       }
    }

    renderSubMenu = (items: ISubMenuItem[] ) => {
       const { chosenTable } = this.props;

       return (
          <SubMenuWrap>
             {items.map(({ title }, idx) => (
                <SubMenuItem
                   key={idx}
                   isActive={title === chosenTable}
                   onClick={() => this.chooseTable(title)}
                >
                   {title}
                </SubMenuItem>
             ))}
          </SubMenuWrap>
       );
    }

    renderMenu = () => {
       return MENU_STRUCTURE.map(({ title, link, disabled, children }, idx) => (
          <MenuItem key={idx} isDisabled={disabled} withSubMenu={children}>
             <a href={link} target="_blank">{title}</a>
             {children && this.renderSubMenu(children)}
          </MenuItem>
       ));
    }

    render() {
       const { isVisible } = this.state;

       return (
          <>
             <MenuToggler onClick={this.toggleMenu} />
             <MenuWrap isVisible={isVisible}>
                {this.renderMenu()}
             </MenuWrap>
          </>
       );
    }
}


const mapStoreToProps = (store: IStore) => ({
   chosenTable: store.chosenTable,
   isMobile: store.isMobile,
});

export const Menu = connect(mapStoreToProps, { chooseTable })(_Menu);
