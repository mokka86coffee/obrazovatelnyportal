import { css } from 'styled-components';

export const flexAlignCenter = css`
  display: flex;
  align-items: center;
  justify-content: flex-start;
`;

export const textEllipsis = css<{ maxWidth?:string }>`
  max-width: ${({ maxWidth = '100%' }) => maxWidth};
  white-space: pre;
  text-overflow: ellipsis;
  overflow: hidden;
`;

export const noDefaultBtnStyles = css`
  padding: 0;
  border: none;
  outline: none;
  cursor: pointer;
  background-color: transparent;
`;

export const replaceDefaultBtnStyles = css`
  ${noDefaultBtnStyles};
  &:active {
    transform: scale(0.98);
  }
`;

export const replaceDefaultLinkStyles = css<{ color?: string }>`
  text-decoration: none;
  &:active {
      ${({ color = '#fff' }) => css`color: ${color};`}
  }
  &:visited {
      ${({ color = '#fff' }) => css`color: ${color};`}
  }
  &:hover {
      ${({ color = '#fff' }) => css`color: ${color};`}
  }
  &:link {
      ${({ color = '#fff' }) => css`color: ${color};`}
  }
`;
