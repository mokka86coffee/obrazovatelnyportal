import { ActionTypes, IAction } from 'app/store/actions/types';
import { AvailableTables } from 'app/store/reducers/types';
import { isMobile } from 'app/store/reducers/utils';
import { IStore } from 'app/store/types';

const initialStore: IStore = {
   isMobile: isMobile(),
   chosenTable: AvailableTables['Распределение приема по направлениям'],
};

export const rootReducer = (store: IStore = initialStore, action: IAction): IStore => {
   switch (action.type) {
      case ActionTypes.CHOOSE_TABLE:
         return Object.assign({}, store, { chosenTable: action.payload });
      default:
         return store;
   }

};
