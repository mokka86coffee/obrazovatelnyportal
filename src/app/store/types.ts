import { ITable } from 'app/components/table/table';
import { AvailableTables } from 'app/store/reducers/types';

export interface ITableData {
    columns: string[];
    data: Array<Record<string, string>>;
    tables?: Record<AvailableTables, ITable>;
    extra?: {
        downloadFileLink?: string;
    }
}

export interface IStore {
    isMobile: boolean;
    chosenTable: AvailableTables;
}
