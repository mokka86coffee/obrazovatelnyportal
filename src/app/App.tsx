import { AvailableTables, Store } from 'app/store/reducers/types';
import { IStore, ITableData } from 'app/store/types';
import React, { Component } from 'react';
import { Header, Footer, TableView } from 'app/components';
import { connect } from 'react-redux';
import tableData from 'static/data/data.json';
import { Container, LineWithGerb, GerbImg, GerbText, DownloadFileButton } from './ui';
import './global-styles.css';

interface IProps {
    downloadFileLink?: string;
}

class _App extends Component<IProps> {
   render() {
      const { downloadFileLink } = this.props;

      return (
         <Container>
            <Header />
            <LineWithGerb>
               <GerbImg />
               <GerbText>Добро пожаловать на корпоративный портал Министерства науки и высшего образования РФ</GerbText>
               <DownloadFileButton href={downloadFileLink} target="_blank" download>Скачать</DownloadFileButton>
            </LineWithGerb>
            <TableView />
            <Footer />
         </Container>
      );
   }
}

const mapStateToProps = (store: IStore) => {
   const chosenTable = store[Store.chosenTable] as AvailableTables;
   // @ts-ignore
   const { extra } = tableData[chosenTable] as ITableData;

   return ({ ...extra });
};

export const App = connect(mapStateToProps)(_App);
